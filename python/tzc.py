#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import glob
import json
import ntpath
import os
import readline
import shutil
import subprocess
import time
import webbrowser
from datetime import datetime
from json import JSONDecodeError
from pathlib import Path
from shlex import split
from subprocess import STDOUT

import click
import rich
from clk import atexit, run
from clk.config import config
from clk.decorators import argument, flag, group, option, use_settings
from clk.lib import (call, check_output, echo_json, json_file, read,
                     safe_check_output)
from clk.log import get_logger
from clk.types import DynamicChoice
from distlib.compat import raw_input
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter
from prompt_toolkit.validation import Validator
from pydantic import BaseModel
from rich import print_json
from rich.panel import Panel
from rich.pretty import Pretty
from rich.progress import Progress
from rich.table import Table

LOGGER = get_logger(__name__)


class SmartPyCli:
    clk_install_script_path = Path(
        __file__).parent.parent / 'install-smartpy.sh'
    base_dir = Path.home() / 'smartpy-cli/'
    script_path = base_dir / 'SmartPy.sh'

    @classmethod
    def install(cls):
        call(['chmod', 'u+x', cls.clk_install_script_path])
        click.echo(check_output([cls.clk_install_script_path]))


class Contract(BaseModel):
    name: str
    value: str


class Account(BaseModel):
    name: str
    value: str


class OctezClient:
    name = "octez_client"
    clk_install_script_path = Path(
        __file__).parent.parent / 'install-octez-client.sh'
    base_dir = Path.home() / '.tezos-client/'
    mutex_path = (base_dir / "clk-mutex")
    config_path = base_dir / 'config'

    @property
    def config(self):
        return (json.loads(self.config_path.read_text())
                if self.config_path.exists() else None)

    @property
    def endpoint(self):
        return self.config["endpoint"] if self.config is not None else None

    contracts_path = base_dir / 'contracts'
    contracts = ([
        Contract(**contract)
        for contract in json.loads(contracts_path.read_text())
    ] if contracts_path.exists() else [])
    secret_keys_path = base_dir / 'secret_keys'
    public_keys_hashs_path = base_dir / 'public_key_hashs'
    accounts = ([
        Account(**account)
        for account in json.loads(public_keys_hashs_path.read_text())
    ] if public_keys_hashs_path.exists() else [])
    account_names = [account.name for account in accounts] if accounts else []
    public_keys_path = base_dir / 'public_keys'

    def __init__(self):
        # see https://tezostaquito.io/docs/rpc_nodes/ and https://teztnets.xyz/ to
        # populate this
        config.globalpreset_profile.settings["tezos_node_candidate"] = {
            "kathmandu": [
                'https://rpc.tzkt.io/kathmandunet',
            ],
            "hangzhou": [
                'https://hangzhounet.api.tez.ie',
                'https://hangzhounet.smartpy.io/',
            ],
            "ghost": [
                "https://rpc.ghostnet.teztnets.xyz",
            ],
            "jakarta": [
                'https://jakartanet.ecadinfra.com',
                'https://rpc.jakartanet.teztnets.xyz/',
                'https://jakartanet.tezos.marigold.dev/',
            ],
            "kathmandu": ["https://rpc.kathmandunet.teztnets.xyz"],
        }

    def check_current_node(self):
        if self.endpoint is not None and self.endpoint not in self.node_candidates:
            LOGGER.warn(
                f"The currently configured endpoint ({self.endpoint})"
                " is not known in the candidates"
                f" for the currently configured network ({config.octez_client.network})."
                f" (Hint: `clk tzc --network {config.octez_client.network}"
                f" node add-candidate {self.endpoint}` to remove this warning)"
            )

    def check_mutex(self):
        if self.mutex_path.exists():
            raise click.UsageError("You cannot run clk tzc twice"
                                   " at the same time"
                                   " because it uses octez-client that"
                                   " does not like it")

    def set_mutex(self):
        self.mutex_path.write_text("")

        @atexit.register
        def release_mutex():
            self.release_mutex()

    def release_mutex(self):
        if self.mutex_path.exists():
            os.unlink(self.mutex_path)

    @property
    def network_candidates(self):
        return config.globalpreset_profile.settings[
            "tezos_node_candidate"].keys()

    @property
    def node_candidates(self):
        return config.settings2.get("tezos_node_candidate",
                                    {}).get(self.network, [])

    @classmethod
    def install(cls):
        call(['chmod', 'u+x', cls.clk_install_script_path])
        click.echo(check_output([cls.clk_install_script_path]))

    def connect(self, node_url):
        LOGGER.status(f'Configuring octez-client network with {node_url}')
        call(["octez-client", "--endpoint", node_url, "config", "update"],
             stderr=subprocess.DEVNULL)

    def import_secret_key(self, alias, secret_key, force=False):
        if not secret_key.startswith("unencrypted:"):
            secret_key = "unencrypted:" + secret_key
        args = ["octez-client", "import", "secret", "key", alias, secret_key]
        if force:
            args += ["--force"]
        call(args)


class NodeSuggestion(DynamicChoice):

    def choices(self):
        return config.octez_client.node_candidates

    def convert(self, value, param, ctx):
        return value


class NetworkChoice(DynamicChoice):

    def choices(self):
        return config.settings2.get("tezos_node_candidate").keys()


class Tzc:
    nft_path = Path('./nft.tzc.json')
    export_path = Path('./account.tzc.json')
    vault_backup_path = Path(__file__).parent.parent \
        / 'vault-backup.template.json'
    nft_sample_path = Path(__file__).parent.parent \
        / 'nft.sample.json'


def safe_json_read_array(path):
    r = read(path)
    if r:
        return json.loads(r)
    else:
        LOGGER.debug(
            f"No content for path={path}, returning empty array in place")
        return []


def safe_json_read_object(path):
    try:
        r = read(path)
    except OSError:
        r = None
    if r:
        return json.loads(r)
    else:
        LOGGER.debug(
            f"No content for path={path}, returning empty object in place")
        return {}


def echo_list(elems, **kwargs):
    res = sorted(elems,
                 key=kwargs['sort_key']) if 'sort_key' in kwargs else elems
    res = '\n'.join(list(map(kwargs['formatter'],
                             res))) if 'formatter' in kwargs else res
    click.echo(res)


def echo_obj(elems, **kwargs):
    res = '\n'.join(list(
        map(kwargs['formatter'],
            elems.items()))) if 'formatter' in kwargs else elems
    click.echo(res)


def stream_command(command):
    process = subprocess.Popen(command,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    rc = None
    while True:
        output = process.stdout.readline()
        if not output and process.poll() is not None:
            break
        if output:
            rich.print(output.decode("utf-8").rstrip())
            rc = process.poll()
    if rc is None:
        click.echo(process.stderr.read())
        LOGGER.critical(f"Something went wrong with '{' '.join(command)}'."
                        " See the error log above.")
        exit(1)
    elif rc:
        rich.print(
            Panel(
                ":no_entry: " +
                f"{' '.join(command)} :\n\n{process.stderr.read().decode('utf-8')}"
            ))
    else:
        rich.print(
            Panel('[bold green]:heavy_check_mark: [/bold green] ' +
                  ' '.join(command)))

    return rc


def pipe_command(cmd, *cmds):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    processes = [proc]
    print_cmd = ' '.join(cmd)
    for cmd in cmds[:-1]:
        print_cmd = print_cmd + ' | ' + ' '.join(cmd)
        proc = subprocess.Popen(cmd, stdin=proc.stdout, stdout=subprocess.PIPE)
    for p in processes:
        p.wait()
        print_cmd = print_cmd + ' | ' + ' '.join(cmds[-1])
        LOGGER.debug(print_cmd)
    return subprocess.check_output(cmds[-1], stdin=proc.stdout).rstrip()


def validator(valids, error='Not a valid value'):
    return Validator.from_callable(lambda x: x in valids,
                                   error_message=error,
                                   move_cursor_to_end=True)


def tzc_prompt(msg, choices):
    return prompt(msg,
                  completer=WordCompleter(choices),
                  validator=validator(choices),
                  mouse_support=True)


@group()
@flag(
    "--disable-disclaimer/--enable-disclaimer",
    help=("Disables the warning displayed"
          " by the client at startup"
          " when it is not launched on Mainnet"),
)
@option(
    "--octez-client-dir",
    help=("Location where the tezos client configuration are"),
)
@option(
    '--network',
    expose_class=OctezClient,
    type=NetworkChoice(),
    help='The network to use',
    expose_value=True,
    default="ghost",
)
@option(
    '--node-url',
    expose_class=OctezClient,
    type=NodeSuggestion(),
    help='The node to be connected to',
    expose_value=True,
)
def tzc(disable_disclaimer, octez_client_dir, network, node_url):
    """Commands to play with octez-client

    This command aims to make it easier to use the underlying octez-client and smartpy-cli commands.

    To update the extension use `clk extension octez_client update` command

    See:
    \t- Tezos - https://tezos.com \r
    \t- https://tezos.gitlab.io/shell/cli-commands.html
    """
    if disable_disclaimer:
        config.override_env["octez_client_UNSAFE_DISABLE_DISCLAIMER"] = "YES"
    if octez_client_dir:
        config.override_env["octez_client_DIR"] = octez_client_dir
        config.init()
    if node_url and node_url != config.octez_client.endpoint:
        config.octez_client.connect(node_url)


@tzc.command()
@argument("stuff", help="Some stuff to explore")
def explore(stuff):
    "Open an explorer of the configured network for the given stuff"
    config.octez_client.check_current_node()
    webbrowser.open(
        f"https://{config.octez_client.network}.tzstats.com/{stuff}")


@tzc.command()
def ipython():
    """Run some interactive python console to help code this command"""
    config.octez_client.check_current_node()
    t = config.octez_client
    s = SmartPyCli
    import IPython
    dict_ = globals()
    dict_.update(locals())
    IPython.start_ipython(argv=[], user_ns=dict_)


def client_version():
    raw_version = safe_check_output([str(SmartPyCli.script_path), '--version'],
                                    stderr=subprocess.PIPE)
    return raw_version if 'SmartPy Version: ' not in raw_version else raw_version[
        len('SmartPy Version: '):].strip()


def install_base58():
    click.echo(check_output('sudo apt-get --assume-yes install base58'))


def echo_invalid(msg):
    rich.print("[bold red] :no_entry: [/bold red]" + msg)


@tzc.command()
@option(
    '--rpc',
    help='The Tezos Node node to connect',
    type=NodeSuggestion(),
)
@flag("--reinstall-octez-client", help="Force reinstalling octez-client")
def install(rpc, reinstall_octez_client):
    """Install required dependencies such as octez-client and smartpy-cli"""

    def beautifier(x):
        rich.print('[bold green]:heavy_check_mark: [/bold green] ' + x)

    with Progress(transient=True) as progress:

        def progress_setup(f):
            progress.stop()
            f()
            progress.start()

        task1 = progress.add_task("[green]npm...", total=5)

        npm_version = safe_check_output(['npm', '--version'])
        if npm_version:
            beautifier(f"npm {npm_version.strip()}")
        else:
            progress_setup(
                lambda: call(split('sudo apt install --yes nodejs npm')))

        progress.update(task1, advance=1, description="[green]smartpy-cli ...")
        smpy_version = client_version()
        if smpy_version:
            beautifier(f'smartpy-cli {smpy_version.strip()}')
        else:
            progress_setup(SmartPyCli.install)
            progress.update(task1,
                            advance=1,
                            description="[green]octez-client ...")

        tzc_version = safe_check_output(['octez-client', '--version'])
        if tzc_version and not reinstall_octez_client:
            beautifier(f'octez-clients {tzc_version.strip()}')
            if rpc is not None and config.octez_client.endpoint != rpc:
                LOGGER.warning(
                    "It looks like you are upgrading the rpc endpoint"
                    " In case you are trying to use a new network,"
                    " you might want to also reinstall octez-client with"
                    " --reinstall-octez-client.")
        else:
            progress_setup(config.octez_client.install)
            progress.update(
                task1,
                advance=1,
                description="[green]Configuring octez-client endpoint ...")

        if rpc is not None:
            if config.octez_client.endpoint == rpc:
                beautifier(
                    f'octez-client endpoint: {config.octez_client.endpoint}')
            else:
                click.get_current_context().invoke(network_set, rpc_link=rpc)
        else:
            if config.octez_client.endpoint is not None:
                beautifier(
                    f'octez-client endpoint: {config.octez_client.endpoint}')
            else:
                progress.stop()
                rich.print(
                    ':magnifying_glass_tilted_right: https://tezostaquito.io/docs/rpc_nodes/'
                )
                rpc = prompt('Node address : ',
                             completer=WordCompleter(
                                 config.octez_client.node_candidates))
                progress.start()
                progress.update(task1,
                                advance=1,
                                description="[green]base58 ...")

        base58_version = safe_check_output(['which', 'base58'])
        if base58_version:
            beautifier(f'base58 {base58_version}')
        else:
            progress_setup(install_base58)
            progress.update(task1, advance=1)


def find_first_account_by_name(name):
    contracts = safe_json_read_array(
        config.octez_client.public_keys_hashs_path)
    return next(filter(lambda x: x['name'] == name, contracts))


@tzc.group(name="account")
def account():
    """Play with accounts"""
    config.octez_client.check_current_node()
    config.octez_client.check_mutex()
    config.octez_client.set_mutex()


class AccountNameType(DynamicChoice):

    def choices(self):
        return [account.name for account in config.octez_client.accounts]


class AccountNameSuggestion(AccountNameType):

    def convert(self, value, param, ctx):
        return value


class AccountType(AccountNameType):

    def converter(self, value):
        return [
            account for account in config.octez_client.accounts
            if account.name == value
        ][0]


@account.command()
@argument("account",
          type=AccountNameSuggestion(),
          help="The account for which you want the balance")
def get_balance(account):
    """Show the balance of the given account"""
    call(["octez-client", "get", "balance", "for", account])


@account.command(name="create")
@argument("alias", help="The new account alias")
def account_create(alias):
    """Create a new account"""
    call(["octez-client", "gen", "keys", alias])


@account.command(name="delete")
@option("--alias", help="The account alias to delete")
def account_delete(alias):
    """Delete an account"""

    if not alias:
        accounts = safe_json_read_array(config.octez_client.public_keys_path)
        names = list(map(lambda x: x['name'], accounts))
        alias = prompt("Account alias to delete >",
                       completer=WordCompleter(names),
                       validator=validator(names, 'Not a valid alias'),
                       mouse_support=True)
    call(["octez-client", "forget", "address", alias, "-f"])


@account.command(name="reveal")
@option("--alias", help="The account alias to reveal the public key")
def account_reveal(alias):
    """Reveal an account public key"""

    if not alias:
        accounts = safe_json_read_array(config.octez_client.public_keys_path)
        names = list(map(lambda x: x['name'], accounts))
        alias = prompt("Account alias to reveal >",
                       completer=WordCompleter(names),
                       validator=validator(names, 'Not a valid alias'),
                       mouse_support=True)
    stream_command(split(f'octez-client reveal key for {alias}'))


@account.command(name="export")
@option("--account", help="The account name to export")
@option("--verbose", is_flag=True, help="The account name to export")
@option("--force", is_flag=True, help="Overwrite the export account if exists")
def account_export(account, verbose, force):
    """Export accounts keys"""

    accounts = safe_json_read_array(config.octez_client.public_keys_path)
    names = list(map(lambda x: x['name'], accounts))
    if verbose:
        click.get_current_context().invoke(account_show)
    if not account or account not in names:
        account = prompt("Alias to export >",
                         completer=WordCompleter(names),
                         validator=validator(names, 'Not a valid alias'),
                         mouse_support=True)

    account_entry = {}

    contract = get_tzc_config_entry_by_account_name(
        config.octez_client.contracts_path, account)
    if contract:
        account_entry["contract"] = contract

    public_key_hash = get_tzc_config_entry_by_account_name(
        config.octez_client.public_keys_hashs_path, account)
    if public_key_hash:
        account_entry["public_key_hash"] = public_key_hash

    public_key = get_tzc_config_entry_by_account_name(
        config.octez_client.public_keys_path, account)
    if public_key:
        account_entry["public_key"] = public_key

    secret_key = get_tzc_config_entry_by_account_name(
        config.octez_client.secret_keys_path, account)
    if secret_key:
        account_entry["secret_key"] = secret_key

    export = {}
    with open(Tzc.export_path, 'r+') as f:
        try:
            export = json.load(f)
        except JSONDecodeError:
            pass
        if account in export and not force:
            click.echo(
                "Hmm, the account has already been exported, use --force to overwrite existing account "
                "export")
            return
        export[account] = account_entry
        f.seek(0)
        json.dump(export, f, indent=4)


@account.command()
@argument("alias", help="The name to give to the provided secret key")
@argument("key", help="The unencrypted secret key")
def import_secret_key(alias, key):
    """See `clk tzc sandbox flextesa generate-bootstrap-account-command bob` to
    guess one"""
    config.octez_client.import_secret_key(alias, key)


@account.command(name="import",
                 help="Import a clk tzc account to octez-client")
@option("--alias", help="The account alias to import")
@option("--verbose", is_flag=True, help="Show available accounts")
@option("--force", is_flag=True, help="Overwrite the account if exists")
def account_import(alias, verbose, force):
    """Import the specified account in octez-client"""

    exports = safe_json_read_object(Tzc.export_path)
    names = list(exports.keys())
    if verbose:
        table = Table(title="Known exports")
        table.add_column("Alias", justify="right")
        for name in names:
            table.add_row(name)
            rich.print(table)

    if alias not in exports:
        alias = prompt('Account to import > ',
                       completer=WordCompleter(names),
                       validator=validator(exports),
                       mouse_support=True)
        account = exports[alias]
        force_flag = " --force" if force else ""
    with Progress(transient=True) as progress:

        def progress_setup(f):
            progress.stop()
            f()
            progress.start()

        task1 = progress.add_task("[green]contract...", total=3)

        if "contract" in account:
            stream_command(
                split(
                    f'octez-client remember contract {alias} {account["contract"]["value"]}{force_flag}'
                ))

        progress.update(task1,
                        advance=1,
                        description="[green]public_key_hash ...")
        if "public_key_hash" in account:
            stream_command(
                split(
                    f'octez-client add address {alias} {account["public_key_hash"]["value"]}{force_flag}'
                ))

        progress.update(task1, advance=1, description="[green]public_key ...")
        if "public_key" in account:
            stream_command(
                split(
                    f'octez-client import public key {alias} {account["public_key"]["value"]}{force_flag}'
                ))

        progress.update(task1, advance=1, description="[green]secret_key ...")
        if "secret_key" in account:
            stream_command(
                split(
                    f'octez-client import secret key {alias} {account["secret_key"]["value"]}{force_flag}'
                ))


@account.command(name="show")
def account_show():
    """List configured accounts usable by the client."""

    table = Table(title="Known accounts")
    table.add_column("Alias", justify="right")
    table.add_column("Address", justify="left")
    for account in config.octez_client.accounts:
        table.add_row(account.name, account.value)
    if table.rows:
        rich.print(table)
    else:
        rich.print(f":person_shrugging: [i]No accounts [/i]")


@account.command(name="explore")
@argument("account", type=AccountType(), help="The account to explore")
def account_explore(account):
    """Open a well known tezos explorer on the given account"""
    webbrowser.open(
        f"https://{config.octez_client.network}.tzstats.com/{account.value}")


@account.command(
    name="snap",
    help=
    "Make a copy of your existing accounts in your octez-client base directory"
)
def account_snap():
    """Create a copy of known octez-client addresses and keys"""
    tzc_base_dir = config.octez_client.base_dir
    timestamp = datetime.now().strftime("%Y-%m-%dT%H%M%S")

    export_path = os.path.join(tzc_base_dir, timestamp)
    os.makedirs(export_path)

    def snap_copy(name):
        shutil.copyfile(os.path.join(tzc_base_dir, name),
                        os.path.join(export_path, name))

    snap_copy('contracts')
    snap_copy('public_key_hashs')
    snap_copy('public_keys')
    snap_copy('secret_keys')

    rich.print(
        "[bold green] :heavy_check_mark: [/bold green] accounts copied to " +
        export_path)


class NodeCandidateConfig:
    pass


@tzc.command()
@argument("network", type=NetworkChoice(), help="The desired network")
def use_network(network):
    """Configure to which network we will connect"""

    run([
        "tzc", "node", "connect",
        config.settings2["tezos_node_candidate"][network][0],
        "--no-check-candidates"
    ])
    run([
        "--log-level", "error", "parameter", "--global", "set", "tzc",
        "--network", network
    ])


@tzc.group()
@use_settings('tezos_node_candidate', NodeCandidateConfig)
def node():
    """Configure to which node we will connect"""


@node.command()
@argument(
    "node-url",
    help="Tezos Node address",
    type=NodeSuggestion(),
)
@flag(
    "--add-candidate",
    help="Also add this url in the list of candidates",
)
@flag(
    "--no-check-candidates",
    help="Will not ask whether to add the node to candidates",
)
def connect(node_url, add_candidate, no_check_candidates):
    """Make octez-client use that node for future interactions"""
    if not no_check_candidates and not add_candidate and node_url not in config.octez_client.node_candidates and not click.confirm(
            f"The url {node_url} is not known in the candidates"
            f" for {config.octez_client.network}."
            f" (Hint: `clk tzc --network {config.octez_client.network}"
            f" node add-candidate {node_url}` to remove this warning)"
            " Do this anyway?"):
        return
    config.octez_client.connect(node_url)
    LOGGER.info(f'Result:')
    call(["octez-client", "config", "show"], stderr=subprocess.DEVNULL)
    if add_candidate:
        run([
            "tzc",
            "--network",
            config.octez_client.network,
            "node",
            "add-candidate",
            node_url,
        ])


@node.command()
@argument("candidate", help="The url to add to connect to the current network")
def add_candidate(candidate):
    """Add this url as a candidate to connect to the given network"""
    candidates = config.tezos_node_candidate.writable.get(
        config.octez_client.network) or []
    if candidate in candidates:
        raise click.UsageError(f"You cannot add {candidate} twice")
    candidates.append(candidate)
    config.tezos_node_candidate.writable[
        config.octez_client.network] = candidates
    config.tezos_node_candidate.write()


@node.command()
@flag(
    "--all",
    help=("Show all the candidates,"
          " not only those that where explicitely given"),
)
def candidates(all):
    """Show the candidate urls associated to the current network"""
    getter = (config.settings2.get("tezos_node_candidate")
              if all else config.tezos_node_candidate.writable)
    candidates = getter.get(config.octez_client.network) or []
    for candidate in candidates:
        click.echo(candidate)


class NodeCandidateType(DynamicChoice):

    def choices(self):
        return config.settings["tezos_node_candidate"][
            config.octez_client.network]


@node.command()
@argument(
    'candidate',
    help="The candidate to get rid of",
    type=NodeCandidateType(),
)
def remove_candidate(candidate):
    """Remove this candidate"""
    candidates = config.tezos_node_candidate.writable.get(
        config.octez_client.network)
    if candidates is None or candidate not in candidates:
        raise click.UsageError(f"{candidate} is not present")
    candidates.remove(candidate)
    config.tezos_node_candidate.write()


@node.command(name="show")
def network_show():
    """Show octez-client node configuration."""
    config.octez_client.check_current_node()
    config.octez_client.check_mutex()
    config.octez_client.set_mutex()
    if endpoint := config.octez_client.endpoint:
        click.echo(f"Using network: {config.octez_client.network}")
        click.echo(endpoint)
        print_json(
            check_output(['curl',
                          os.path.join(endpoint, 'version'), '-s']))


@account.command(name="transfer")
@argument('source',
          help='The account alias from which the XTZ will be taken',
          type=AccountNameType())
@argument('dest',
          help='The account alias or address to which the XTZ will sent',
          type=AccountNameSuggestion())
@argument('amount', help='The XTZ amount to sent', type=int)
def account_transfer(source, dest, amount):
    """XTZ transfer"""
    stream_command([
        "octez-client", "transfer",
        str(amount), "from", source, "to", dest, "--burn-cap", "0.5"
    ])


@account.command(name='long-secret')
@option('--account', help='The account alias to export')
@click.pass_context
def account_long_secret(ctx, account):
    """Show the account long secret key, made from the private and public key"""
    accounts = safe_json_read_array(config.octez_client.public_keys_path)
    names = list(map(lambda x: x['name'], accounts))
    if not account or account not in names:
        account = prompt("Account alias to export keys > ",
                         completer=WordCompleter(names),
                         validator=validator(names, 'Not a valid alias'),
                         mouse_support=True)
    unencrypted_prefix = 'unencrypted:'

    public_key = get_tzc_config_entry_by_account_name(
        config.octez_client.public_keys_path, account)['value']
    if 'locator' in public_key:
        public_key = public_key['locator']
    if public_key.startswith(unencrypted_prefix):
        public_key = public_key[len(unencrypted_prefix):]

    short_secret_key = get_tzc_config_entry_by_account_name(
        config.octez_client.secret_keys_path, account)['value']
    if short_secret_key.startswith(unencrypted_prefix):
        short_secret_key = short_secret_key[len(unencrypted_prefix):]

    part1 = pipe_command(['echo', short_secret_key], ['base58', '-c', '-d'],
                         ['xxd', '-p', '-c', '1000'],
                         ['sed', 's/^.\{8\}//g']).decode("utf-8")
    if not part1:
        echo_invalid("Can't read short secret key")

    part2 = pipe_command(['echo', public_key], ['base58', '-c', '-d'],
                         ['xxd', '-p', '-c', '1000'],
                         ['sed', 's/^.\{8\}//g']).decode("utf-8")
    if not part2:
        echo_invalid("Can't read public key")
    long_secret = pipe_command(['echo', str(part1 + part2)],
                               ['sed', 's/^/2bf64e07/'], ['xxd', '-r', '-p'],
                               ['base58', '-cc']).decode("utf-8")
    if ctx.parent.info_name == 'account':
        print(long_secret)
    return long_secret


@account.command(name='vault-backup')
@option('--account', help='The account alias to export')
def account_vault_backup(account):
    """Print the Vault admin backup backup command"""
    accounts = safe_json_read_array(config.octez_client.public_keys_path)
    names = list(map(lambda x: x['name'], accounts))
    if not account or account not in names:
        account = prompt("Account alias to export keys > ",
                         completer=WordCompleter(names),
                         validator=validator(names, 'Not a valid alias'),
                         mouse_support=True)
        long_private_key = click.get_current_context().invoke(
            account_long_secret, account=account)
        public_key = get_tzc_config_entry_by_account_name(
            config.octez_client.public_keys_path, account)['value']

    public_key_found = False
    if 'key' in public_key:
        public_key_found = True
        public_key = public_key['key']
    if not public_key_found and type(
            public_key) == str and public_key.startswith("unencrypted:"):
        public_key_found = True
        public_key = public_key.removeprefix("unencrypted:")
    if not public_key_found:
        rich.print(
            Panel(
                ":no_entry: " +
                f"account {account} does not contain value.key field, seems like it has not been activated"
            ))
        return

    encoded_public_key = pipe_command(
        ['echo', public_key],
        ['base58', '-d', '-c'],
        ['xxd', '-p', '-c', '1000'],
        ['sed', 's/^.\{8\}//g'],
        ['xxd', '-r', '-p'],
        ['base64'],
        ['tr', '-d', '\n'],
    ).decode('utf-8')

    encoded_private_key = pipe_command(
        ['echo', long_private_key],
        ['base58', '-d', '-c'],
        ['xxd', '-p', '-c', '1000'],
        ['sed', 's/^.\{8\}//g'],
        ['xxd', '-r', '-p'],
        ['base64'],
        ['tr', '-d', '\n'],
    ).decode('utf-8')

    vault_backup = safe_json_read_object(Tzc.vault_backup_path)
    vault_backup['policy']['keys']['1']['key'] = str(encoded_private_key)
    vault_backup['policy']['keys']['1']['public_key'] = str(encoded_public_key)
    vault_backup['archived_keys']['keys'][1]['key'] = str(encoded_private_key)
    vault_backup['archived_keys']['keys'][1]['public_key'] = str(
        encoded_public_key)
    vault_backup = json.dumps(vault_backup)

    vault_backup = pipe_command(['echo', vault_backup], ['base64'],
                                ['tr', '-d', '\n']).decode('utf-8')
    expected_backup_length = 1984

    if expected_backup_length != len(vault_backup):
        echo_invalid(
            f'Invalid backup length: expected={expected_backup_length}, actual={len(vault_backup)}'
        )
    else:
        print(f'vault write transit/restore/admin backup={vault_backup}')


def path_complete(text, state):
    return (glob.glob(text + '*') + [None])[state]


def prompt_path_autocomplete(msg):
    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(path_complete)
    return raw_input(f'{msg} > ')


def hexencode(str):
    res = str.encode('utf-8').hex()
    return "0x" + res


def build_fa2_storage(admin_address, metadata):
    res = '(Pair ' \
        '(Pair (Pair "' + admin_address + '" 0) '  # admin
    res += '(Pair {} '  # ledger
    res += '(Pair {Elt "" ' + hexencode(metadata) + ' }'  # metadata
    res += '{}))) '  # operator
    res += '(Pair (Pair {} '  # (pair (big_map %owner_by_token_id nat address)
    res += '(Pair {} False))'  # (bool %paused)
    res += ' (Pair {} (Pair {} {}))))'  # %token_metadata %token_info %total_supply
    return res


def get_tzc_config_entry_by_account_name(tzc_config_file_path, name):
    with open(tzc_config_file_path, 'r') as f:
        data = json.load(f)
        if len(data) > 0:
            for entry in data:
                if name == entry["name"]:
                    return entry
    return


def michelson_mint_nft_paramaters(owner_name, nft_tzc_name, token_id):
    """Generate the mint michelson parameter"""
    nfts = safe_json_read_object('./nft.tzc.json')
    if nft_tzc_name in nfts.keys():
        nft = nfts[nft_tzc_name]
    else:
        raise Exception("No such nft ", nft_tzc_name, ", you can try one of ",
                        nfts.keys())
    elements = []

    for attr in nft:
        hexValue = hexencode(nft[attr])
        elements.append(f'Elt "{attr}" {hexValue}')

    #  (pair (map %metadata string bytes) (nat %token_id)))
    token_info = '(Pair {' + '; '.join(elements) + '} ' + str(token_id) + ')'
    owner_address = find_first_account_by_name(owner_name)['value']
    mint_params = f'(Pair (Pair "{owner_address}" 1) {token_info} )'

    return mint_params


@tzc.group(name="contract")
def contract():
    """Play with contracts"""
    config.octez_client.check_current_node()
    config.octez_client.check_mutex()
    config.octez_client.set_mutex()


class ContractNameType(DynamicChoice):

    def choices(self):
        return [contract.name for contract in config.octez_client.contracts]


class ContractType(ContractNameType):

    def converter(self, value):
        return [
            contract for contract in config.octez_client.contracts
            if contract.name == value
        ][0]


def get_contract_names():
    contracts = safe_json_read_array(config.octez_client.contracts_path)
    return list(map(lambda x: x['name'], contracts))


@contract.command(name="show")
@option("--alias",
        help="Show the contract address for the given alias",
        type=ContractType())
def contract_show(alias):
    """List known contracts alias and address"""

    if alias:
        return click.echo(alias.value)

    table = Table(title="Known contracts")
    table.add_column("Alias", justify="right")
    table.add_column("Address", justify="left")
    for c in config.octez_client.contracts:
        table.add_row(c.name, c.value)
    if table.rows:
        rich.print(table)
    else:
        rich.print(f":person_shrugging: [i]No accounts [/i]")


@contract.command(name="explore")
@argument("contract", type=ContractType(), help="The contract to explore")
def contract_explore(contract):
    """Open a well known tezos explorer on the given contract"""
    webbrowser.open(
        f"https://{config.octez_client.network}.tzstats.com/{contract.value}")


@contract.command(name="add")
@option("--alias", help="The alias of the contract", type=ContractNameType())
@option("--address", help="The address of the contract")
@option("--force", is_flag=True, help="Force the import if exists")
def contract_add(alias, address, force):
    """Add a contract alias for the given address"""
    if not alias:
        alias = prompt("Alias >")
    if not address:
        address = prompt("Address >")
        force = '--force' if force else ''
        stream_command(
            split(f'octez-client remember contract {alias} {address} {force}'))


@contract.command(name="remove")
@argument("alias",
          help="The alias of the contract to remove",
          type=ContractNameType())
def contract_remove(alias):
    """Remove a contract alias"""
    with json_file(config.octez_client.contracts_path) as j:
        j = [contract for contract in j if contract["name"] != alias]


# TODO options
@contract.command(name="deploy-fa2")
@option('--force', is_flag=True, help='Overwrite alias if already exists')
def contract_fa2_deploy(force):
    """FA2 contract deployment"""
    accounts = safe_json_read_array(config.octez_client.public_keys_hashs_path)
    account_names = list(map(lambda x: x['name'], accounts))

    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(path_complete)
    contract_path = raw_input('Smart-contract path > ')

    contract_alias = prompt('Give an alias to this contract > ')
    source_account = tzc_prompt('Admin > ', account_names)
    transfer_qty = 0

    contract_filename = ntpath.basename(contract_path)
    call([
        SmartPyCli.script_path, 'compile', contract_path,
        f'./compile/{contract_filename}'
    ])
    contract_dir = [
        f.path for f in os.scandir("./compile/" + contract_filename)
        if f.is_dir()
    ][0]
    contract_code = contract_dir + "/step_000_cont_0_contract.tz"
    # FIXME hard coded IPFS
    init_storage = build_fa2_storage(
        find_first_account_by_name(source_account)['value'],
        "ipfs://QmaJEkhFnFQCwZA3uYWZq3LYvHw4s8RQtEc8sjpWQyJAKp")

    command = [
        "octez-client", "originate", "contract", contract_alias,
        "transferring",
        str(transfer_qty), "from",
        find_first_account_by_name(source_account)['value'], "running",
        contract_code, "--init", f'{init_storage}', "--burn-cap",
        str(10)
    ]
    if force:
        command.append('--force')
    stream_command(command)


@tzc.group()
def nft():
    """Play with NFT"""
    config.octez_client.check_current_node()
    config.octez_client.check_mutex()
    config.octez_client.set_mutex()


@nft.command(name='mint')
@option('--contract', help='The smart-contract alias that will own the NFT')
@option('--admin', help='The smart-contract account alias')
@option('--owner', help='The account that will own the NFT')
@option('--nft-alias', help='The NFT alias to mint')
@option('--token-id', help='The NFT token id')
def nft_mint(contract, admin, owner, nft_alias, token_id):
    """Mint NFT for a given contract"""
    if not os.path.exists(Tzc.nft_path):
        rich.print(
            Panel(f":no_entry: File not found {Tzc.nft_path}.\n"
                  " This file is expected to have a structure like the"
                  " following one:"))
        print_json(read(Tzc.nft_sample_path))
        return 1
    contract_names = get_contract_names()
    if contract and contract not in contract_names:
        echo_invalid(f'Unknown contract={contract}')
        contract = None

    account_names = config.octez_client.account_names
    if admin and admin not in account_names:
        echo_invalid(f'Unknown admin={admin}')
        admin = None

    nft_names = safe_json_read_object(Tzc.nft_path).keys()
    if nft_alias and nft_alias not in nft_names:
        echo_invalid(f'Unknown nft alias={nft_alias}')
        nft_alias = None

    if not contract:
        contract = tzc_prompt('Contract > ', contract_names)
    if not admin:
        admin = tzc_prompt('Contract admin > ', account_names)
    if not owner:
        # TODO Allow to provide a literal or an alias
        owner = tzc_prompt('NFT owner > ', config.octez_client.account_names)
    if not nft_alias:
        nft_alias = tzc_prompt('NFT name > ', list(nft_names))

    if not token_id:
        token_id = int(prompt('Token Id: '))

    args = michelson_mint_nft_paramaters(owner, nft_alias, token_id)

    stream_command([
        "octez-client", "call", contract, "from", admin, "--entrypoint",
        "mint", "--arg", args, "--burn-cap",
        str(0.5)
    ])


@nft.command(name='transfer')
@option('--contract', help='The contract owning the NFT')
@option('--contract-admin', help='The admin of the contract')
@option('--prev-owner', help='The previous nft owner account address')
@option('--next-owner', help='The next nft owner account address')
@option('--token-id', help='The nft token id')
def nft_transfer(contract, contract_admin, prev_owner, next_owner, token_id):
    """Transfer nft"""
    contract_names = get_contract_names()
    if contract and contract not in contract_names:
        echo_invalid(f"Not a valid contract={contract}")
        contract = None

    account_names = config.octez_client.account_names
    if contract_admin and contract_admin not in account_names:
        echo_invalid(f"Not a valid admin={contract_admin}")
        contract_admin = None

    if not contract_admin:
        contract_admin = tzc_prompt('Contract admin > ', account_names)
        contract_admin = find_first_account_by_name(contract_admin)['value']
    if not contract:
        contract = tzc_prompt('Contract > ', contract_names)

    yes_no_completer = WordCompleter(['y', 'n'])

    transfers = []
    if prev_owner and next_owner and token_id:
        # TODO Handle alias
        transfers.append(
            f'Pair "{prev_owner}" {{Pair "{next_owner}" (Pair {token_id} 1) }}'
        )
    else:
        add_transfer = True
        while add_transfer:
            from_account = prompt('From (account or literal): ',
                                  completer=WordCompleter(account_names))
            if from_account in account_names:
                from_account = find_first_account_by_name(
                    from_account)['value']

            to_account = prompt('To (account or literal): ',
                                completer=WordCompleter(account_names))
            if to_account in account_names:
                to_account = find_first_account_by_name(to_account)['value']

            token_id = int(prompt("Token ID: "))

            qty = int(prompt("Quantity: ", default='1'))
            transfers.append(
                f'Pair "{from_account}" {{Pair "{to_account}" (Pair {token_id} {qty}) }}'
            )
            add_transfer = prompt('Transfer another (y/n) ?',
                                  completer=yes_no_completer) == 'y'

    args = '{' + '; '.join(transfers) + '}'
    stream_command([
        "octez-client", "call", contract, "from", contract_admin,
        "--entrypoint", "transfer", "--arg", args, "--burn-cap",
        str(0.5)
    ])


@nft.command(name='show')
def nft_show():
    """Show the known TZC nft templates"""
    nfts = safe_json_read_object(Tzc.nft_path)
    table = Table(title="Known NFTs")
    table.add_column("Alias", justify="right")
    table.add_column("Metadata", justify="left")

    for name in nfts:
        table.add_row(name, Pretty(nfts[name]))
    if table.rows:
        rich.print(table)
    else:
        rich.print(
            f":person_shrugging: [i]No NFTs found at {Tzc.nft_path}[/i]")


@tzc.group()
def sandbox():
    """Manipulate sandboxes"""


class StoppedFlextesa(Exception):
    pass


class FlexTesa:

    def __init__(self):
        self._container_name = None

    @property
    def container_name(self):
        if self._container_name is None:
            self._container_name = f"clk-tezos-{config.octez_client.network}"
        return self._container_name

    @container_name.setter
    def container_name(self, value):
        self._container_name = value

    def generate_bootstrap_account_command(self, name):
        alias, public_key, address, secret_key = check_output([
            "docker", "run", "--rm", "--name", "generator",
            "oxheadalpha/flextesa", "flextesa", "key-of-name", name
        ]).strip().split(",")
        return {
            "alias": alias,
            "public_key": public_key,
            "address": address,
            "secret_key": secret_key,
        }

    def dump_generated_account(self, generated):
        return f'{generated["alias"]},{generated["public_key"]},{generated["address"]},{generated["secret_key"]}'

    def logs(self):
        return check_output(["docker", "logs", self.container_name],
                            stderr=STDOUT)

    @property
    def is_running(self):
        return json.loads(
            check_output(["docker", "inspect",
                          self.container_name]))[0]["State"]["Running"]

    def kill(self):
        call(["docker", "kill", self.container_name])

    def wait(self):
        while "Waiting for N000 (1) to reach level" not in self.logs():
            LOGGER.status("Waiting for flextesa to start")
            if not self.is_running:
                raise StoppedFlextesa()
            time.sleep(1)


@sandbox.group()
@option("--container-name",
        expose_class=FlexTesa,
        help="The name to use for the docker container")
def flextesa():
    """Use flextesa to provide sandboxing"""


@flextesa.command()
@argument("name", help="The name of the account you want to have boostrapped")
@flag("--import-into-octez-client",
      help="Put this secret key into octez-client instead of showing it here")
@flag("--force", help="Overwrite the existing account if need be")
def generate_bootstrap_account_command(name, import_into_octez_client, force):
    """Generate a bootstrap account string"""
    generated = config.flextesa.generate_bootstrap_account_command(name)
    if import_into_octez_client:
        config.octez_client.import_secret_key(
            generated["alias"],
            generated["secret_key"],
            force=force,
        )
    else:
        echo_json(generated)


@flextesa.command()
@option("--port", help="The port to expose", default=18731)
@option("--block-time", help="The time each block has", default=3)
@flag("--follow", "--tail", help="Show the log as well")
@flag("--force", help="Kill the sandbox before starting it")
@option("--bootstrap-account",
        help="An account to provide in the sandbox",
        multiple=True)
@option("--bootstrap-alias",
        help="An alias to generate an account and bootstrap it",
        multiple=True)
@flag("--import-into-octez-client",
      help="Put this secret key into octez-client instead of showing it here")
@flag("--no-wait/--wait", help="Don't wait for it to be ready")
def start(port, block_time, follow, force, bootstrap_account, bootstrap_alias,
          import_into_octez_client, no_wait):
    """Start a flextesa using the appropriate network"""
    bootstrap_account = list(bootstrap_account)
    bootstrap_alias = list(bootstrap_alias)
    for already_present in "alice", "bob":
        if already_present in bootstrap_alias:
            LOGGER.warn(
                f"{already_present} is already in flextesa, I will ignore it")
            bootstrap_alias.remove(already_present)
    for alias in bootstrap_alias:
        generated = config.flextesa.generate_bootstrap_account_command(alias)
        if import_into_octez_client:
            config.octez_client.import_secret_key(generated["alias"],
                                                  generated["secret_key"])
        bootstrap_account.append(
            config.flextesa.dump_generated_account(generated))

    container_name = config.flextesa.container_name
    already_running = False
    if container_name in check_output(
            split('docker ps --format {{.Names}}')).split():
        if force:
            call(["docker", "kill", container_name])
        else:
            LOGGER.warning("This sandbox is already running."
                           " I won't do anything."
                           " Use --force to delete it before running it again")
            already_running = True
    if not already_running:
        args = [
            "docker", "run", "--detach", "--rm", "--name", container_name,
            "-p", f"{port}:20000", "-e", f"block_time={block_time}",
            "oxheadalpha/flextesa", f"{config.octez_client.network}box",
            "start"
        ]
        for account in bootstrap_account:
            args = args + [
                "--add-bootstrap-account", f"{account}@2_000_000_000_000"
            ]
        call(args)
    if follow:
        run(["tzc", "sandbox", "flextesa", "logs", "--follow"])
    if not no_wait:
        LOGGER.info("Waiting for the sandbox to be ready")
        run(["tzc", "sandbox", "flextesa", "wait"])


@flextesa.command()
def stop():
    """Stop the flextesa docker container"""
    config.flextesa.kill()


@flextesa.command(flowdepends=["tzc.sandbox.flextesa.start"])
def wait():
    """Wait for the flextesa docker container to be ready"""
    config.flextesa.wait()


@flextesa.command()
@flag("--follow", "-f", help="Don't exit")
def logs(follow):
    """Show the logs of the running flextesa docker container"""
    container_name = config.flextesa.container_name
    args = ["docker", "logs", container_name]
    if follow:
        args.append("--follow")
    call(args)
