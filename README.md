# clk tezos-client

While playing with the [tezos](https://tezos.com/) blockchain, **clk tezos-client** aims to ease the use of the [tezos-client](https://tezos.gitlab.io/shell/cli-commands.html) command.

The goal is to offer:

* Use interactive commands with auto-completion.
* Allow both usage of address or alias.
* Make easier to share your keys (_ex: when working on testnet in a team_)
* Use JSON for token metadata-description

## Setup and configuration

* Either:
    * If you do not use `clk` yet: install [clk-project](https://github.com/clk-project) and the
      extension using the single following command:
      ```bash
      $ curl -sSL https://clk-project.org/install.sh | env CLK_EXTENSIONS=xdev-tech/tezos_client bash
      ```
    * Or if you already have [clk-project](https://github.com/clk-project), you can simply install
      this extension with
      ```bash
      $ clk extension install xdev-tech/tezos_client
       ```

* Execute `clk tzc install`


The installation process will end by prompting an RPC address, it shows
you [a link](https://tezostaquito.io/docs/rpc_nodes/) where available community nodes can be found.

(**FIXME** : let the user choose by network rather that prompting for an RPC, like done for `clk tzc use-network` command)

- Create the tezos-client config directory if it doesn't exist :
  ```bash
  $ mkdir <home>/.tezos-client
  ```
  (**FIXME** : automate this step in `$ clk tzc install`)
- Set up the desired network :
  ```bash
  $ clk tzc use-network <network>
  ```
  (**FIXME** : this step should be useless as we already asked for which network to use after the installation process)


### Update the exension
Use `clk tzc --update-extension`

## User flows
### Create an account with tezzies

To create a new account use

```
clk tzc account create your-alias
```

The command `clk tzc account show` will show the wallet address you just created.

To get tezzies go to [teztnets.xyz](https://teztnets.xyz/), click on the faucet link of the desired network, copy your newly created address and request tezzies.

### Deploy a FA2 contract

You will need :
- an [account with tezzies](#create-an-account-with-tezzies)
- a FA2 contract. 
  
  Fortunately, there is one ready in the [smartcontracts repo](https://gitlab.com/the-blockchain-xdev/xdev-sandbox/smartcontracts) at `smartcontract/tezos/identity/FA2_batch_mint.py`.
  
  You can clone this repo to retrieve the smartcontract.


Once this is done, run
```
clk tzc contract deploy-fa2
```
When prompted :
- enter the path to the smartcontract mentioned earlier (or any smartcontract you wish).
- enter an alias for the contract
- select the alias that will be the admin

After a few seconds the command should succeed. You can list this command to list your contracts :
```
clk tzc contract show
```

### Minting an NFT

You will need :
- an [account with tezzies](#create-an-account-with-tezzies)
- a [deployed FA2 contract](#deploy-a-fa2-contract)
- a `nft.tzc.json` file containing NFT's metadatas. There is one in the smartcontract repo at `smartcontract/tezos/nft.tzc.json`.

Run this command (you need to be in the directory containing the `nft.tzc.json` file) :
```
clk tzc nft mint
```
When prompted enter :
- the contract alias you just deployed
- the alias of the contract admin
- the alias of the account that will own the NFT
- the NFT name
- the token ID (current token ID + 1)

After a few seconds the command should succeed, you can verify that on an indexer.

### Using another network

To use another network use :
```
clk tzc use-network
```
and use tab to show the networks list to choose from.

After running this command, any subsequents uses of `clk tzc` will be made on the chosen network. 

## Common commands

* Transfer XTZ from an account to another account : `clk tzc account transfer`
* Contract deployment: `clk tzc contract deploy-fa2`
* Mint NFT: `clk nft mint`

  The command will prompt you the required information.

  **NB:** For the _NFT owner_ input, currently you need to provide an account known by the
  tezos-client. If you need to make another account as the nft provider, first make a known address
  as the owner and then use the `clk tzc nft transfer` command.

* Transfer NFT: `clk tzc nft transfer`

## Nota bene

* This is an early experimental version. :scream:

  => Update the extension: `clk extension update tezos_client`
* Feel free to kindly open issues. :kissing_heart:
* Improve the tool :sunglasses:

## TODO

* [ ] Create a lib module or make an MR on clk project
* [ ] Handle dry-run
* [ ] Install without the `install-smartpy.sh` script
* [ ] Command for Smartpy-cli version
* [ ] Uninstall Smartpy-cli:

```bash
ifneq ($(shell test -s ~/smartpy-cli || echo false), false)
	rm -r ~/smartpy-cli
	echo "smartpy-cli successfully uninstalled!"
else
	echo "smartpy-cli is not installed"
endif
```

* [ ] Command `update` = `uninstall` + `install`

* [ ] Uninstall tezos-client:

```bash
ifneq ($(shell test -s ~/.local/bin/tezos-client || echo false), false)
	rm ~/.local/bin/tezos-client
	echo "tezos-client successfully uninstalled!"
else
	echo "tezos-client is not installed"
endif
```

* [ ] Auto-completion for ZSH
* [ ] Add an option to define a network rpc addresse bases only on the network name
