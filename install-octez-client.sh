#!/bin/bash
TMP="$(mktemp -d)"
trap "rm -rf '${TMP}'" 0

pushd "${TMP}" > /dev/null
{
    wget https://github.com/serokell/tezos-packaging/releases/download/v16.1-1/binaries-16.1-1.tar.gz
    tar xf binaries-16.1-1.tar.gz
    chmod +x octez-client
    mkdir -p $HOME/.local/bin
    mv octez-client $HOME/.local/bin
    if ! command -v octez-client >/dev/null
    then
        echo 'export PATH="$HOME/.local/bin:$PATH"' >> $HOME/.bashrc
    fi
}
popd > /dev/null
